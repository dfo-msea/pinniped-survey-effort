# Import modules
import sys
import logging
from datetime import datetime
import traceback
import glob
from osgeo import gdal, ogr
import geopandas as gp
import pandas as pd
import numpy as np
import networkx as nx
import rasterio as rio
from shapely.ops import transform, nearest_points
from shapely.geometry import Point, LineString
from geopandas.tools import sjoin
from scipy.spatial import cKDTree
from tqdm import tqdm
import requests
import os
import math
from beautifultable import BeautifulTable
import contextily as ctx
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

# set up global variables
# date string
DATE_TIME = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
# log name
LOG_NAME = "survey-logfile"
# log string
LOG_STRING = f"LOGGING {LOG_NAME} - {DATE_TIME}"
# Message break string
MSG_BREAK = '+--------------------------------------------------------------+'
SUBAREA_BREAK = '################################################################'


# https://stackoverflow.com/questions/34954373/disable-format-for-some-messages
class ConditionalFormatter(logging.Formatter):
    def format(self, record):
        if hasattr(record, 'simple') and record.simple:
            return record.getMessage()
        else:
            return logging.Formatter.format(self, record)


def setup_logger(directory, name=LOG_NAME, date=DATE_TIME, level=logging.INFO):
    """Gets or creates a logger.

    Args:
        directory (str): Directory path for output file
        name (str, optional): Name for logfile. Defaults to LOG_NAME.
        date (datetime, optional): Date to append to logfile name. Defaults to DATE_TIME.
        level (str, optional): Logging level for file. Defaults to logging.INFO.

    Returns:
        logger_obj: Logger object.
    """
    # Gets or creates a logger
    logger_obj = logging.getLogger(name)

    # set log level
    logger_obj.setLevel(level)

    # Create log subdirectory or set the path if it exists.
    logdir = make_dir(directory, 'logs')

    # define file handler and set formatter
    file_handler = logging.FileHandler(os.path.join(logdir, f"{name}-{date}.log"))
    formatter = ConditionalFormatter("%(asctime)s:%(levelname)s:%(module)s(%(lineno)d) - %(message)s")
    file_handler.setFormatter(formatter)

    # add file handler to logger
    logger_obj.addHandler(file_handler)
    return logger_obj


def make_dir(outdir, outname):
    """Make a directory if it does not exist.

    Args:
        outdir: Base directory.
        outname: Name for dir to create in base dir.

    Returns: path to new directory.

    """
    try:
        outpath = os.path.join(outdir, outname)
        # Make output directory if it does not exist already
        if not os.path.exists(outpath):
            print(f"Creating directory: {outpath}")
            os.mkdir(outpath)
        else:
            print(f"{outpath} already exists.")
        return outpath
    except Exception:
        logging.error(traceback.format_exc())


logger = setup_logger('../')


def load_data(data_dir, subdir, file_ext):
    """Load data as geopandas dataframe.

    Args:
        data_dir: data directory.
        subdir: subdirectory within data directory.
        subdir: subdirectory name from data dir.
        file_extension: file extension (i.e., 'csv', 'gpkg')

    Returns: GeoPandas GeoDataFrame object.

    """
    # Get name of file matching extension.
    logger.info(MSG_BREAK)
    subdir_path = os.path.join(data_dir, subdir)
    logger.info(f'Searching for data of type {file_ext} in {subdir_path}')
    filename = glob.glob(os.path.join(subdir_path, f'*.{file_ext}'))
    if len(filename) > 1:
        logger.error(f'More than one input file found for filetype: {file_ext} in {subdir_path}')
        sys.exit()
    elif len(filename) == 0:
        logger.error(f'No input file found for filetype: {file_ext} in {subdir_path}')
        sys.exit()
    else:
        logger.info(f'Loading {filename[0]} into GeoDataFrame.')
        gdf = gp.read_file(filename[0])
        logger.info(f'Number of records in GeoDataFrame: {len(gdf)}')
        return gdf


# Check CRS and reproject
def check_reproject_crs(gdf, out_epsg):
    """ Check the CRS of GeoDataFrame. Reproject if necessary.

    Args:
        gdf: GeoDataFrame
        epsg: Integer for EPSG code.

    Returns: New Geodataframe with specified projection.

    """
    logger.info(MSG_BREAK)
    out_epsg = int(out_epsg)
    src_crs = gdf.crs
    if src_crs is None:
        logger.info(
            f'No Coordinate Reference System (CRS) detected, assigning CRS to GeoDataFrame using EPSG:{out_epsg}.')
        gdf = gdf.set_crs(f'EPSG:{out_epsg}')
        return gdf

    src_epsg = int(src_crs.to_epsg())
    logger.info(f'GeoDataframe EPSG:{src_epsg}')
    if src_epsg != out_epsg:
        logger.info(f'Reprojecting GeoDataFrame to EPSG:{out_epsg}')
        return gdf.to_crs(f'EPSG:{out_epsg}')
    else:
        logger.info('GeoDataFrame CRS already matches specified projection code.')
        return gdf


def download_file(url, outdir, outname):
    """Download file from online source.

    Args:
        url (str): URL to file to download.
        outdir (str): Output directory path.
        outname (str): Output file name.

    Returns:
        str: Output file path.
    """
    # Streaming, so we can iterate over the response.
    logger.info('Downloading file...')
    response = requests.get(url, stream=True)
    total_size_in_bytes = int(response.headers.get('content-length', 0))
    block_size = 1024  # 1 Kibibyte
    progress_bar = tqdm(total=total_size_in_bytes, unit='iB', unit_scale=True)
    outfile = os.path.join(outdir, outname)
    with open(outfile, 'wb') as file:
        for data in response.iter_content(block_size):
            progress_bar.update(len(data))
            file.write(data)
    progress_bar.close()
    if total_size_in_bytes != 0 and progress_bar.n != total_size_in_bytes:
        logger.error("ERROR: something went wrong")
    return outfile


# Flatten geometry https://github.com/shapely/shapely/issues/709
def _to_2d(x, y, z):
    return tuple(filter(None, [x, y]))


# https://gis.stackexchange.com/questions/222315/finding-nearest-point-in-other-geodataframe-using-geopandas
def ckdnearest(gdA, gdB):
    """ Get 10 nearest track points and the datetime stamp for each.

    Args:
        gdA: GeoDataFrame with source geometry.
        gdB: GeoDataFrame with destination geometry (find nearest points).

    Returns:

    """
    nA = np.array(list(gdA.geometry.apply(lambda x: (x.x, x.y))))
    nB = np.array(list(gdB.geometry.apply(lambda x: (x.x, x.y))))
    btree = cKDTree(nB)
    # Get 10 nearest neighbours
    dist, idx = btree.query(nA, k=10)
    # Subset the nearest neighbours records, sort by time
    # and get the index of the record with the earliest time
    nearest_idx_list = [gdB.iloc[idx_list].sort_values('time').index[0] for idx_list in idx]
    # Get times from list of indices
    track_times = gdB.iloc[nearest_idx_list]['time']
    return track_times


def get_buffer_value(gdf_points, gdf_lines, gdf_poly, cell_res):
    # Get points in land poly
    poly_union = gdf_poly.geometry.unary_union
    logger.info('Getting points on land to calculate distance to shoreline and buffer value...')
    on_land = gdf_points[gdf_points.geometry.within(poly_union)]
    if len(on_land) > 0:
        logger.info(f'{len(on_land)} haulouts on land. Calculating distance to shoreline.')
        min_dist = np.empty(len(on_land))
        for i, point in enumerate(on_land['geometry']):
            min_dist[i] = np.min([point.distance(line) for line in gdf_lines['geometry']])
        on_land['min_dist_to_lines'] = min_dist
        # Get buffer value and multiply by 1.05 to account for conversion to raster grid cell
        buffer_value = roundup((on_land['min_dist_to_lines'].max()) * 1.05)
        # math.ceil to round up and then up to nearest 10
        logger.info(f'Buffer value set at {buffer_value}')
        return buffer_value
    else:
        logger.info(f'No points on land. Setting buffer value at {cell_res} to match cell resolution.')
        return cell_res


def move_points(gdf_points, gdf_lines, gdf_poly):
    # Get points in land poly
    # https://gis.stackexchange.com/questions/306838/snap-points-shapefile-to-line-shapefile-using-shapely
    logger.info('Searching for points on land...')
    gdf_points = sjoin(gdf_points, gdf_poly, how='left')
    num_on_land = len(gdf_points[gdf_points['index_right'] == 0.0])
    shply_line = gdf_lines.geometry.unary_union
    if num_on_land > 0:
        # https://stackoverflow.com/questions/23330654/update-a-dataframe-in-pandas-while-iterating-row-by-row
        logger.info(f'{num_on_land} haulouts on land. Calculating nearest location on line...')
        for i, row in gdf_points.iterrows():
            if row['index_right'] == 0.0:
                nearest = nearest_points(shply_line, row['geometry'])[0]
                gdf_points.at[i, 'geometry'] = nearest
        return gdf_points
    else:
        logger.info(f'No points on land.')
        return gdf_points


def roundup(x):
    """Round a number up to nearest 10.
    https://stackoverflow.com/questions/26454649/python-round-up-to-the-nearest-ten

    Args:
        x: number

    Returns: number rounded up to nearest 10

    """
    return int(math.ceil(x / 10.0)) * 10


def get_subareas_list(regions_gdf, subareacode=None):
    """Get a list of subarea codes in the specified region polygon.

    Args:
        regions_gdf: GeoDataFrame with the Region already subset.
        subareacode: A single subarea within the Region to process.

    Returns: List of subarea codes in the region to process.

    """
    logger.info(MSG_BREAK)
    subarea_list = []
    if subareacode is not None:
        logger.info(f'User specified a single subarea code: {subareacode} from region.')
        if subareacode not in list(regions_gdf['SubareaCode']):
            logger.error(f'{subareacode} is not a value in the SubareaCode field for the region.')
            sys.exit()
        subarea_list.append(subareacode)
    else:
        logger.info('Getting list of all subarea codes in region.')
        subarea_list = list(regions_gdf['SubareaCode'])
    logger.info(f'{len(subarea_list)} subareas to process.')
    return subarea_list


def vector_to_raster(polygon, pixel_size, output_dir, output_epsg_code, subarea_poly, subarea_name,
                     burn_value, nodata_value, coast=False):
    """
    Convert a vector polygon to a raster GeoTIFF. Output filename will have the same name as input file (besides
    the extension). Project GeoTiff to specified projection using EPSG code
    :param polygon: in-memory GeoDataFrame of coast polygon
    :param pixel_size: desired cell size for output GeoTIFF
    :param output_dir: output directory
    :param output_epsg_code: epsg code for GeoTIFF
    :param subarea_poly: in-memory GeoDataFrame of subarea polygon
    :param subarea_name: name of subarea
    :param burnvalue: value to burn into raster
    :param nodata_value: NoData value for raster
    :param coast: If True, append coast to filename
    :return: filepath to output raster
    """
    logger.info(MSG_BREAK)
    logger.info('PROCESSING VECTOR TO RASTER...')

    # Open the vector data source and read in the extent
    # https://stackoverflow.com/questions/42206195/use-geodataframe-as-a-osgeo-ogr-datasource
    # Pass the GeoDataFrame into ogr as GeoJson
    source_ds = ogr.Open(polygon.to_json())
    source_layer = source_ds.GetLayer()
    subarea_ds = ogr.Open(subarea_poly.to_json())
    subarea_layer = subarea_ds.GetLayer()
    # get extent of layer
    x_min, x_max, y_min, y_max = subarea_layer.GetExtent()

    # Filename of the raster GeoTIFF that will be created (temporary file and projected file)
    temp_filename = os.path.join(output_dir, 'temp.tif')
    if coast:
        projected_filename = os.path.join(output_dir, f'{subarea_name}_coast_binary.tif')
    else:
        projected_filename = os.path.join(output_dir, f'{subarea_name}_binary.tif')
    # Set parameters for destination data source
    logger.info('Setting parameters for output raster')
    x_res = int((x_max - x_min) / pixel_size)
    y_res = int((y_max - y_min) / pixel_size)

    # Create the destination data source
    target_ds = gdal.GetDriverByName('GTiff').Create(temp_filename, x_res, y_res, 1, gdal.GDT_Byte)
    target_ds.SetGeoTransform((x_min, pixel_size, 0, y_max, 0, -pixel_size))

    # assign nodata value to raster band
    logger.info(f'Assigning NoData value as {nodata_value} for output raster')
    band = target_ds.GetRasterBand(1)
    band.SetNoDataValue(nodata_value)

    # Rasterize vector layer (osgeo.ogr.Layer)
    logger.info('Converting to raster')
    gdal.RasterizeLayer(target_ds, [1], source_layer, burn_values=[burn_value])

    # Close dataset to write to disk
    target_ds = None

    # Project raster to EPSG <code>
    epsg_code = f'EPSG:{output_epsg_code}'
    logger.info(f'Projecting raster to {epsg_code}')
    land_binary = gdal.Open(temp_filename)
    gdal.Warp(projected_filename, land_binary, dstSRS=epsg_code)
    # Calculate statistics on raster band
    logger.info('Calculating statistics for GeoTIFF')
    proj_binary = gdal.Open(projected_filename)
    proj_band = proj_binary.GetRasterBand(1)
    proj_band.GetStatistics(0, 1)

    # Close dataset and write to disk
    land_binary = None

    # Cleanup temp file
    os.remove(temp_filename)
    logger.info('PROCESSING VECTOR TO RASTER... COMPLETE')
    return projected_filename


def proximity_raster(binary_path, output_dir, subarea_name):
    """Create a proximity raster where the cell values represent the distance to a land cell from a binary raster.

    Args:
        binary_path: Path to binary raster file
        output_dir: Output directory
        subarea_name: Subarea name

    Returns: path to proximity raster

    """
    # https://gis.stackexchange.com/questions/300286/geodesic-distance-from-raster-in-python
    # Create a distance to land raster
    logger.info(MSG_BREAK)
    logger.info(f'Creating proximity raster for {subarea_name.upper()}...')
    src_ds = gdal.Open(binary_path)
    srcband = src_ds.GetRasterBand(1)
    dst_filename = os.path.join(output_dir, f'{subarea_name}_proximity.tif')

    drv = gdal.GetDriverByName('GTiff')
    dst_ds = drv.Create(dst_filename,
                        src_ds.RasterXSize, src_ds.RasterYSize, 1,
                        gdal.GetDataTypeByName('Float32'))

    dst_ds.SetGeoTransform(src_ds.GetGeoTransform())
    dst_ds.SetProjection(src_ds.GetProjectionRef())

    dstband = dst_ds.GetRasterBand(1)

    # Calculate proximity of each cell to land cell (value==1)
    gdal.ComputeProximity(srcband, dstband, ["VALUES=1", "NODATA=-9999.0", "DISTUNITS=PIXEL"])
    srcband = None
    dstband = None
    src_ds = None
    dst_ds = None

    # Calculate statistics on raster band
    logger.info('Calculating statistics for GeoTIFF')
    proj_prox = gdal.Open(dst_filename)
    proj_band = proj_prox.GetRasterBand(1)
    proj_band.GetStatistics(0, 1)
    proj_prox = None
    proj_band = None
    return dst_filename


def raster2array(raster_path):
    """
    Convert a raster to an array. Assumes single-band raster and will only return first band if more than 1.
    :param raster_path: path to raster file
    :return: object of class type numpy.ndarray
    """
    raster = gdal.Open(raster_path)
    band = raster.GetRasterBand(1)
    array = band.ReadAsArray()
    return array


# Create graph from array and subset based on binary values
def grid2graph_subset(binary_array, proximity_array, subarea_name):
    """Create a Graph of nodes and edges (networkx) using the shape attribute of array.
    Subset the Graph by removing nodes where the corresponding array value == 1. Keep only 0 values.

    :param binary_array: binary array
    :param proximity_array: proximity array
    :param subarea_name: subarea name
    :return: subset of Graph where values == 1 from the input array are excluded (land values)
    """
    logger.info(MSG_BREAK)
    logger.info(f'Generating graph for {subarea_name}, removing nodes on land...')
    logger.info(f'Adding proximity values to nodes as "weight" attribute...')
    # generate graph - same shape as array
    G = nx.grid_2d_graph(*binary_array.shape)
    # remove those nodes where the corresponding binary value is != 0
    for i, binary_val, prox_value, node in zip(tqdm(binary_array.ravel()),
                                               binary_array.ravel(),
                                               proximity_array.ravel(),
                                               sorted(G.nodes())):
        if binary_val != 0:
            G.remove_node(node)
        else:
            # Add proximity value as weight
            G.nodes[node]['weight'] = prox_value
    return G


def add_edge_weights(network_graph):
    """Generate edge weights based on weight values in nodes

    :param array:
    :return: Graph with weighted edges
    """
    logger.info(MSG_BREAK)
    logger.info('Adding edge weights to graph...')
    for i, edge in zip(tqdm(network_graph.edges), network_graph.edges()):
        w1 = network_graph.nodes[edge[0]]['weight']
        w2 = network_graph.nodes[edge[1]]['weight']
        # Calculate average weight using weights from two nodes connecting the edge
        edge_weight = sum([w1, w2]) / 2
        network_graph[edge[0]][edge[1]]['weight'] = edge_weight
    return network_graph


def get_coord_list(gdf):
    """
    Get a list where each entry is a tuple of tuples such as...
    [((src_x, src_y), (dst_x, dst_y)),
    ((src_x, src_y), (dst_x, dst_y))]
    https://stackoverflow.com/questions/23151246/iterrows-pandas-get-next-rows-value
    """
    logger.info(MSG_BREAK)
    logger.info('Creating list of geographic coordinates from haulout locations...')
    row_iterator = gdf.iterrows()
    _, last = next(row_iterator)
    coords_list = []
    for i, (index, row) in enumerate(row_iterator):
        # Exclude last haulout since it is the final destination
        if i == len(gdf) - 1:
            pass
        else:
            src_coords = (last['geometry'].x, last['geometry'].y)
            dst_coords = (row['geometry'].x, row['geometry'].y)
            coords = (src_coords, dst_coords)
            coords_list.append(coords)
            last = row
    return coords_list


def pixel_coords(raster_filename, coordinates):
    """
    Get pixel coordinates from projected coordinates of source and destination points
    :param raster_filename:
    :param coordinates: tuple of coordinates ((source x, source y), (dest x, dest y))
    :return: list of pixel coordinates
    """

    # coords list of tuples (coords)
    coords_list = []

    # Open the raster
    with rio.open(raster_filename) as dataset:
        # Loop through your list of coords
        for i, (x_coord, y_coord) in enumerate(coordinates):
            # Get pixel coordinates from map coordinates
            pixel_coords = (dataset.index(x_coord, y_coord))
            coords_list.append(pixel_coords)
    return coords_list


def make_layered_map(*args):
    """The first arg is the output directory. Second arg is the subarea name.
    This function accepts an arbitrary number of geodataframes, plots them on top of a Contextily basemap.
    NOTE: Please edit the Plotting-section to specify parameters for the number of layers and the formatting of each layer.
    Output: Saved file and layered map for display.
    https://medium.com/geekculture/plotting-maps-with-geopandas-and-contextily-8d4b1f02603d"""

    # Convert the CRS for all layers to EPSG3857 to match Contextily
    gdfs = list(map(lambda x: x.to_crs(epsg=3857), args[4:]))
    # Create figure
    fig, ax = plt.subplots(1, figsize=(25, 25))
    # Set aspect to equal
    ax.set_aspect('equal')

    outdir = args[0]
    subarea = args[1].upper()
    percent_surveyed = args[2]
    survey_year = args[3]
    outpng = os.path.join(outdir, subarea + '_' + survey_year + '.png')

    # PLOTTING: Specify layers to plot how to format each layer (colours, transparency, etc.):
    # Layer 1: buffered path
    gdfs[0].plot(ax=ax, color='lightyellow', edgecolor='orange', hatch="/", alpha=0.5, zorder=1,
                 label='Buffered Survey Path (Estimated)')
    # Layer 2: unsurveyed path
    gdfs[1].plot(ax=ax, color='blue', linewidth=2, alpha=1.0, zorder=2, label='Unsurveyed Low Water Line')
    # Layer 3: network path
    gdfs[2].plot(ax=ax, color='black', linewidth=3, alpha=0.8, zorder=3, label='Survey Path (Estimated)')
    # Layer 4: haulout locations
    # Create a dictionary where you assign each attribute value to a particular color
    pointPalette = {'Haulout': 'red',
                    'Anchor': 'green'}
    # https://www.earthdatascience.org/courses/scientists-guide-to-plotting-data-in-python/plot-spatial-data/customize-vector-plots/python-customize-map-legends-geopandas/
    # Loop through each attribute type and plot it using the colors assigned in the dictionary
    for ctype, data in gdfs[3].groupby('Type'):
        # Define the color for each group using the dictionary
        color = pointPalette[ctype]

        # Plot each group using the color defined above
        data.plot(color=color,
                  ax=ax,
                  label=ctype,
                  markersize=150,
                  alpha=0.6,
                  zorder=4
                  )
    # Layer 5: subarea boundary
    gdfs[4].boundary.plot(ax=ax, edgecolor='black', linestyle='dashdot', linewidth=5, alpha=0.5, zorder=5,
                          label=subarea)

    # Contextily basemap:
    ctx.add_basemap(ax, source=ctx.providers.Esri.WorldGrayCanvas)

    # Turn off axis
    ax.axis('off')
    ax.legend()
    plt.title(f'{subarea} ({survey_year}): {percent_surveyed}% Surveyed', fontsize=25)
    # Save as file
    fig.savefig(outpng, dpi=300)  # Comment out or delete if you don't want a PNG image saved
    plt.close()
    return outpng


def numpy_replace(envelope_path, subarea_path):
    logger.info('Replacing values in binary raster...')
    envelope_ds = gdal.Open(envelope_path)
    # Open in writing mode
    subarea_ds = gdal.Open(subarea_path, 1)
    # Read as numpy arrays
    envelope_array = envelope_ds.ReadAsArray()
    subarea_array = subarea_ds.ReadAsArray()
    # Get only 1's from envelope array
    con1 = (envelope_array == 1)
    # Replace value with 1 where envelope value was 1
    subarea_array[con1] = 1

    band = subarea_ds.GetRasterBand(1)
    band.WriteArray(subarea_array)
    band.FlushCache()

    del band, envelope_ds, subarea_ds

    return subarea_path


def process(regioncode, epsgproj, landbuffer, pathbuffer, cellres, subareacode=None, anchor=False):
    """
    regioncode='SOG'
    epsgproj=3005
    landbuffer=-50
    pathbuffer=500
    cellres=40
    subareacode='HOWESD'
    anchor
    """
    logger.info('Starting script...', extra={'simple': True})
    logger.info('>>>>>>>>>>PARAMETERS<<<<<<<<<<', extra={'simple': True})
    param_table = BeautifulTable()
    param_table.columns.header = ['regioncode', 'epsgproj', 'landbuffer', 'pathbuffer', 'cellres', 'subareacode',
                                  'anchor']
    param_table.rows.append([regioncode, epsgproj, landbuffer, pathbuffer, cellres, subareacode, anchor])
    logger.info(param_table, extra={'simple': True})
    logger.info('\n')
    # Move up one directory level.
    data_dir = os.path.join('../', 'data')
    output_dir = os.path.join(data_dir, 'output')
    region_outdir = make_dir(output_dir, regioncode + '_' + DATE_TIME)
    # Load haulouts.
    haulouts = load_data(data_dir, 'haulouts', 'csv')
    haulouts_gdf = gp.GeoDataFrame(haulouts, geometry=gp.points_from_xy(haulouts.Longitude, haulouts.Latitude))
    haulouts_4326 = check_reproject_crs(haulouts_gdf, 4326)
    haulouts_epsgproj = check_reproject_crs(haulouts_4326, epsgproj)
    haulouts_epsgproj['Type'] = 'Haulout'
    if anchor:
        # Load pseudopoints.
        pseudo = load_data(data_dir, 'pseudopoints', 'csv')
        pseudo_gdf = gp.GeoDataFrame(pseudo, geometry=gp.points_from_xy(pseudo.Longitude, pseudo.Latitude))
        pseudo_4326 = check_reproject_crs(pseudo_gdf, 4326)
        pseudo_epsgproj = check_reproject_crs(pseudo_4326, epsgproj)
        pseudo_epsgproj['Type'] = 'Anchor'
    # Load Coast Line and Coast Polygon.
    coastline = load_data(data_dir, os.path.join('coastline', 'line'), 'shp')
    coastline_epsgproj = check_reproject_crs(coastline, epsgproj)
    coastpoly = load_data(data_dir, os.path.join('coastline', 'poly'), 'shp')
    coastpoly_epsgproj = check_reproject_crs(coastpoly, epsgproj)
    # Load regions with subareas.
    regions = load_data(data_dir, 'regions', 'gpkg')
    regions_epsgproj = check_reproject_crs(regions, epsgproj)
    # Subset region using RegionCode.
    regions_epsgproj = regions_epsgproj.loc[regions_epsgproj['RegionCode'] == regioncode]
    # Load track points.
    trackpoints = load_data(data_dir, os.path.join('trackpoints', regioncode), 'gpkg')
    trackpoints_epsgproj = check_reproject_crs(trackpoints, epsgproj)
    if trackpoints_epsgproj.iloc[0].geometry.has_z:
        # Flatten trackpoint geom (3D to 2D).
        logger.info('Flattening track point geometry from 3D to 2D (removing elevation from geometry)...')
        tracks_geom2D = [transform(_to_2d, row.geometry) for row in trackpoints_epsgproj.itertuples()]
        # Create new dataframe with 2D geometry.
        trackpoints_epsgproj = trackpoints_epsgproj.set_geometry(tracks_geom2D)

    subareas = get_subareas_list(regions_epsgproj, subareacode)
    # Create empty dataframe for results
    column_names_results_df = ['RegionCode',
                               'SubareaCode',
                               'year',
                               'Subarea_length_metres',
                               'Subarea_percent_surveyed',
                               'Surveyed_length_metres']
    results_df = pd.DataFrame(columns=column_names_results_df)

    for subarea in tqdm(subareas):
        # Lowercase string for files.
        subarea_name = subarea.lower()
        logger.info(SUBAREA_BREAK)
        logger.info(f'PROCESSING {subarea} SUBAREA.')
        logger.info(SUBAREA_BREAK)
        subarea_outdir = make_dir(region_outdir, subarea)
        # Subset haulouts by subarea.
        haulouts_subset = haulouts_epsgproj.loc[haulouts_epsgproj['SubareaCode'] == subarea]
        logger.info(f'{len(haulouts_subset)} haulouts in {subarea} subarea.')
        # Get list of unique years the subarea was surveyed
        unique_years = sorted(haulouts_subset['year'].unique())

        if anchor:
            # Subset pseudopoints by subarea.
            pseudo_subset = pseudo_epsgproj.loc[pseudo_epsgproj['SubareaCode'] == subarea]
            # Duplicate anchor points for each survey year
            pseudo_subset_repeated = gp.GeoDataFrame(pd.concat([pseudo_subset] * len(unique_years), ignore_index=True),
                                                     crs=f'EPSG:{epsgproj}')
            # Repeat years by number of pseudo_subset records
            years_list = [[year] * len(pseudo_subset) for year in unique_years]
            # Flatten nested lists into single list of repeated years
            flat_list = [item for sublist in years_list for item in sublist]
            pseudo_subset_repeated['year'] = flat_list

            # Combine geodataframes (haulouts and pseudopoints)
            logger.info(
                f'Adding {len(pseudo_subset_repeated)} records to haulouts dataframe. {len(pseudo_subset)} anchor '
                f'point(s) * number of unique year(s) ({len(unique_years)}).')
            haulouts_subset = gp.GeoDataFrame(pd.concat([haulouts_subset, pseudo_subset_repeated], ignore_index=True),
                                              crs=f'EPSG:{epsgproj}')

        # Group haulouts by year
        haulouts_grouped = haulouts_subset.groupby('year')
        # Get subarea record
        subarea_record = regions_epsgproj[regions_epsgproj['SubareaCode'] == subarea]
        # Negative buffer the subarea polygon
        subarea_buffer_df = {'Name': subarea, 'geometry': subarea_record.geometry.buffer(landbuffer)}
        subarea_buffer_gdf = gp.GeoDataFrame(subarea_buffer_df, crs=f'EPSG:{epsgproj}')
        # Create envelope and clip by subarea
        temp_df = {'Name': subarea, 'geometry': subarea_buffer_gdf.envelope}
        temp_gdf = gp.GeoDataFrame(temp_df, crs=f'EPSG:{epsgproj}')
        subarea_difference = gp.overlay(temp_gdf, subarea_buffer_gdf, how='difference')
        # Convert subarea to binary
        binary_subarea = vector_to_raster(subarea_difference,
                                          cellres,
                                          subarea_outdir,
                                          epsgproj,
                                          subarea_buffer_gdf,
                                          subarea_name,
                                          1,
                                          -9999,
                                          coast=False)

        # Clip coast polygon by subarea
        coast_poly_clip = gp.clip(coastpoly_epsgproj, subarea_record)
        # Clip coastline by subarea
        coast_line_clip = gp.clip(coastline_epsgproj, subarea_record)
        # Negative buffer the clipped polygon
        coast_poly_clip_buff = coast_poly_clip.buffer(landbuffer)
        # Convert clipped coastpoly into binary raster (1=land, 0=water)
        binary_raster = vector_to_raster(coast_poly_clip_buff,
                                         cellres,
                                         subarea_outdir,
                                         epsgproj,
                                         subarea_buffer_gdf,
                                         subarea_name,
                                         1,
                                         -9999,
                                         coast=True)
        # Replace the areas outside the subarea boundary with 1's instead of 0's
        binary_raster = numpy_replace(binary_subarea, binary_raster)
        # Create distance to shore raster
        prox_raster = proximity_raster(binary_raster,
                                       subarea_outdir,
                                       subarea_name)
        # Create numpy array from binary raster
        binary_array = raster2array(binary_raster)
        # Create numpy array from proximity raster
        proximity_array = raster2array(prox_raster)
        water_graph = grid2graph_subset(binary_array, proximity_array, subarea)
        water_graph = add_edge_weights(water_graph)
        # Empty list for haulout sites
        haulout_sites = []
        for survey_year in tqdm(unique_years):
            # Get haulout records for that year
            haulouts_year = haulouts_grouped.get_group(survey_year)
            # Get list of haulout Subsite ID values
            haulout_sites_current = haulouts_year['SubsiteID'].unique()
            # Process on first iteration, or if different SiteID values are in current year compared to previous year
            if len(haulout_sites) == 0 or (set(haulout_sites_current) != set(haulout_sites)):
                logger.info(MSG_BREAK)
                logger.info(f'Processing survey year: {survey_year}')
                logger.info(MSG_BREAK)
                # Make copy of points rather than a view
                haulouts_year = haulouts_year.copy()
                # Check if there is only 1 haulout point
                if len(haulouts_year) <= 1:
                    logger.warning('Fewer than 2 sites available. Cannot create path without at least 2 points.')
                    logger.warning(f'Skipping analysis for {survey_year}.')
                    continue
                # Make year subdirectory
                year_outdir = make_dir(subarea_outdir, 'survey_' + survey_year)
                # Get datetime values from earliest record from nearest 10 points
                track_times = ckdnearest(haulouts_year, trackpoints_epsgproj)
                # Assign datetime values to column in dataframe
                haulouts_year.insert(1, 'nearest_track_time', track_times.values)
                # Order haulouts by TIME columns from track points
                haulouts_year = haulouts_year.sort_values('nearest_track_time')
                # Insert column for order of sites visited ordered by datetime stamp
                # https://stackoverflow.com/questions/38862293/how-to-add-incremental-numbers-to-a-new-column-using-pandas/38862389
                haulouts_year.insert(0, 'order', range(1, 1 + len(haulouts_year)))
                # Move haulout locations that are on land to nearest location on coast line
                haulouts_moved = move_points(haulouts_year, coast_line_clip, coast_poly_clip)
                haulouts_outfile = os.path.join(year_outdir, f'{subarea_name}_haulouts.shp')
                haulouts_moved.to_file(haulouts_outfile)
                coords_list = get_coord_list(haulouts_moved)
                # For each tuple of src and dst coords (in lat/lng), get pixel coords (xy indices in array)
                logger.info('Creating list of pixel coordinates from binary raster at each haulout location...')
                pix_coords = [pixel_coords(binary_raster, coords) for coords in coords_list]
                # get list of indices not in graph
                haulout_indices = [ptuple for plist in pix_coords for ptuple in plist if
                                   ptuple not in water_graph.nodes]
                # If there are nodes not in graph, move to next subarea.
                if len(haulout_indices) > 0:
                    logger.warning(
                        f'There area haulouts that have no corresponding node in the graph. Skipping subarea {subarea}.'
                        'This indicates that the landside buffer needs to be increased so that the haulout'
                        'is on a water node.')
                    new_row = {'RegionCode': regioncode,
                               'SubareaCode': subarea,
                               'year': survey_year,
                               'Subarea_length_metres': 0,
                               'Surveyed_length_metres': 0,
                               'Subarea_percent_surveyed': 0}
                    # append row to the dataframe
                    results_df = results_df.append(new_row, ignore_index=True)
                    continue

                # https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.shortest_paths.generic.shortest_path.html#networkx.algorithms.shortest_paths.generic.shortest_path
                # https://stackoverflow.com/questions/59343417/how-to-get-the-shortest-path-in-a-weighted-graph-with-networkx/59343472
                logger.info(f'Generating paths for {subarea}...')
                paths = [nx.shortest_path(G=water_graph, source=src_dest[0], target=src_dest[1], weight='weight') for
                         src_dest
                         in pix_coords]
                # Open binary raster to get xy coords
                binary_raster_ds = rio.open(binary_raster)
                # For each path, get values in path and get the lat/lng for each node.
                logger.info('Getting geographic coordinates for each node in path...')
                logger.info(MSG_BREAK)
                logger.info(f'Generating shapefiles for ordered haulouts, paths, and buffered paths in {subarea}...')
                paths_coords = [binary_raster_ds.xy(val[0], val[1]) for path in paths for val in path]
                # Close dataset
                binary_raster_ds.close()
                # Create points from list of path coordinates
                c = [Point(coord[0], coord[1]) for coord in paths_coords]
                # Create line from points
                l = LineString(c)
                d = {'Name': [subarea], 'geometry': [l]}
                # Create geodataframe from line data
                df = gp.GeoDataFrame(d, crs=f'EPSG:{epsgproj}')
                df_buffer = df.copy(deep=True)
                # Write line to disk
                line_outfile = os.path.join(year_outdir, f'{subarea_name}_path.shp')
                df.to_file(line_outfile)
                # Write haulouts to disk (with ordered data)
                haulouts_outfile = os.path.join(year_outdir, f'{subarea_name}_haulouts.shp')
                haulouts_moved.to_file(haulouts_outfile)
                # Buffer path polyline.
                logger.info(f'Buffering path polyline for {subarea} subarea.')
                df_buffer['geometry'] = df_buffer.buffer(pathbuffer)
                # Write buffered path to disk.
                pathbuffer_outfile = os.path.join(year_outdir, f'{subarea_name}_pathbuffer_{pathbuffer}.shp')
                df_buffer.to_file(pathbuffer_outfile)
                # Erase the buffered path from clipped coastline.
                coast_difference = gp.overlay(coast_line_clip, df_buffer, how='difference')
                coast_length = sum(coast_line_clip.length)
                unsurveyed_length = sum(coast_difference.length)
                surveyed_length = coast_length - unsurveyed_length
                percent_surveyed = round((surveyed_length / coast_length) * 100, 2)
                difference_outfile = os.path.join(year_outdir, f'{subarea_name}_unsurveyed.shp')
                coast_difference.to_file(difference_outfile)
                new_row = {'RegionCode': regioncode,
                           'SubareaCode': subarea,
                           'year': survey_year,
                           'Subarea_length_metres': round(coast_length, 2),
                           'Surveyed_length_metres': round(surveyed_length, 2),
                           'Subarea_percent_surveyed': percent_surveyed}
                # append row to the dataframe
                results_df = results_df.append(new_row, ignore_index=True)
                logger.info(MSG_BREAK)
                # Plot results
                logger.info(f'Plotting results for {subarea} subarea.')
                logger.info(MSG_BREAK)
                layered_map = make_layered_map(year_outdir,
                                               subarea_name,
                                               percent_surveyed,
                                               survey_year,
                                               df_buffer,
                                               coast_difference,
                                               df,
                                               haulouts_moved,
                                               subarea_record)
            else:
                logger.info(f'Haulout sites for {survey_year} are same as previous surveyed year... skipping year')
                same_msg = 'Same as previous survey'
                new_row = {'RegionCode': regioncode,
                           'SubareaCode': subarea,
                           'year': survey_year,
                           'Subarea_length_metres': same_msg,
                           'Surveyed_length_metres': same_msg,
                           'Subarea_percent_surveyed': same_msg}
                # append row to the dataframe
                results_df = results_df.append(new_row, ignore_index=True)
            haulout_sites = haulout_sites_current

    results_csv_file = os.path.join(region_outdir, 'historical_survey_results.csv')
    results_df.to_csv(results_csv_file, sep=',', index=False)
    results_table = BeautifulTable()
    results_table.columns.header = results_df.columns
    for i, row in results_df.iterrows():
        results_table.rows.append(row)
    logger.info('>>>>>>>>>>RESULTS<<<<<<<<<<', extra={'simple': True})
    logger.info(results_table, extra={'simple': True})
