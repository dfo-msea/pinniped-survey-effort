import argparse
import survey_functions as survey
import os


def main(inargs):
    """
    Run the program.
    :param inargs: command line arguments from argparse.
    :return:
    """
    working_directory = os.getcwd()
    survey.process(inargs.regioncode,
                   inargs.epsgproj,
                   inargs.landbuffer,
                   inargs.pathbuffer,
                   inargs.cellres,
                   inargs.subareacode,
                   inargs.anchor)


if __name__ == '__main__':
    description = 'Calculate proportion of coastline covered during surveys for defined regions/subareas for ' \
                  'historical pinniped aerial surveys.'
    parser = argparse.ArgumentParser(description=description)

    # Add arguments.
    parser.add_argument('regioncode', type=str, default=None,
                        choices=['CMC', 'DPASS', 'HG', 'NMC', 'QCS', 'SOG', 'WCVI'],
                        help='Pinniped Research Program RegionCode to run the program on, i.e. SOG.'
                             'This will load the tracklines data from the matching subdirectory in the tracklines '
                             'data directory.')
    parser.add_argument('epsgproj', type=int, default=3005,
                        help='Numeric EPSG code for output data (e.g., 3005). Should be in a projected coordinate '
                             'system (not geographic) due to distance calculations and buffer analyses.')
    parser.add_argument('landbuffer', type=int, default=-40,
                        help='Integer to buffer land polygon by (e.g., -40). A negative value is required in order to '
                             'ensure that haulout locations are located on "water" nodes.')
    parser.add_argument('pathbuffer', type=int, default=500,
                        help='Integer to buffer network path by (e.g., 500). The buffered path is used to erase '
                             'sections of the coastline, essentially marking them as surveyed.')
    parser.add_argument('cellres', type=int, default=30,
                        help='Integer used to assign spatial resolution (cell size) for output raster GeoTIFFs. '
                             'The binary raster is used to generate the graph and therefor a higher resolution will '
                             'mean more nodes and edges to use for the analysis.')
    parser.add_argument('--subareacode', type=str, default=None,
                        help='Subset analysis by specifying a subarea within the region polygon.')
    parser.add_argument('--anchor', action='store_true',
                        help='If --anchor is specified, analyses will be run by appending anchor points '
                             'to haulout points to guide path development.')

    args = parser.parse_args()

    main(args)
