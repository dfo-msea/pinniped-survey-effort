# Pinniped Survey Effort

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: N/A


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
Estimate the percent of shoreline covered in historical aerial surveys.


## Summary
The Pinniped Research Program (PRP) require rough estimates for the percent of shoreline covered in past aerial surveys for 
part of a CSAS process. Modern aerial surveys include GPS data of the flight path as the plane moves from one haulout site to the next. 
However, there is no GPS track data for historical surveys, making it difficult to reliably estimate the percent of shoreline 
covered for each survey year. Using GPS points from a modern survey, the order of haulout sites (and anchor points used to 
supplement the haulouts) visited in past surveys is estimated. Then, a path is created from a weighted graph (using the haulout sites 
and anchors as nodes, and proximity to land values as edge weights in the graph) connecting the haulout sites and anchor points. 
The path is buffered, and the coastline is erased using the buffered path. The results include a csv file with the percent of 
coast line estimated for each subarea in the region, and for each year the survey was conducted (if the haulouts visited were different 
than the previous survey year). An image file is also produced, plotting the subarea boundary, haulout sites and anchors, the low water line 
clipped to the subarea, the estimated path, and the buffered path.

| Data | Filename | Input/Output | Source | Manually Created | Edited | Format |
|:---|:---|:---|:---|:---|:---|:---|
| Low Water Line | CHS_LWL_cleaned.shp | Input | CHS | No | Yes | Shapefile |
| Low Water Polygon | CHS_LWL_cleaned_polyclip.shp | Input | Low Water Line | Yes | Yes | Shapefile |
| Haulouts survey data | haulout list_assesment data 1966-2012_UPDATE_2022-01-07.csv | Input | PRP | No | No | csv |
| Pseudopoints | Pseudopoints_for_path_reconstruction.csv | Input | PRP | Yes | No | csv |
| PRP Regions | Pv_Subareas_1file_2021_refresh_PFO.Borders_validS2.gpkg | Input | PRP | No | No | GeoPackage |
| PRP Modern Track Points | 2019_SOG_TrackPts.gpkg | Input | PRP | No | No | GeoPackage |
| Subarea(s) Binary Raster | <subareaname>_binary.tif | Output | Script | No | No | GeoTIFF |
| Subarea(s) Proximity Raster | <subareaname>_proximity.tif | Output | Script | No | No | GeoTIFF |
| Subarea(s) & Year(s) Haulouts | <subareaname>_haulouts.shp | Output | Script | No | No | Shapefile |
| Subarea(s) & Year(s) Estimated Network Paths | <subareaname>_path.shp | Output | Script | No | No | Shapefile |
| Subarea(s) & Year(s) Buffered Network Paths | <subareaname>\_pathbuffer_<distance>.shp | Output | Script | No | No | Shapefile |
| Subarea(s) & Year(s) Estimated Unsurveyed Coastline | <subareaname>_unsurveyed.shp | Output | Script | No | No | Shapefile |
| Plot | <subareaname>_<year>.png | Output | Script | No | No | PNG |


## Status
In-development.


## Contents
This is the directory structure expected for running the code. Results are written to disk in the `/data/output` directory.
```
project
└───code
│   │   README.md
│   │   run.py
|   |   survey_functions.py  
|
└───data
|   └───coastline
|       └───line
|       |      CHS_LWL_cleaned.shp 
|       └───poly
|       |      CHS_LWL_cleaned_polyclip.shp
|   └───haulouts
|       |      haulout list_assesment data <specific-version>.csv
|   └───output
|       └───<region>
|       |   └───<subarea>
|       |       └───survey<year>
|       |       |       <subarea>_haulouts.shp
|       |       |       <subarea>_path.shp
|       |       |       <subarea>_pathbuffer<distance>.shp
|       |       |       <subarea>_<year>.png
|       |       |       <subarea>_unsurveyed.shp
|       |   <region>_binary.tif
|       |   <region>_proximity.tif
|       |      
│       │
|   └───pseudopoints
|       |      Pseudopoints_for_path_reconstruction-<specific-version>.csv
|   └───regions
│       │      <regions-subareas>.gpkg
|   └───trackpoints
|       |   └───<subarea>
                │   
                └───<subareacode>
                |       <trackpoints>.gpkg
 
```

## Methods
- Input data are loaded as GeoDataframes and re-projected to specified Coordinate Reference System (CRS) using the epsgproj 
argument. 
- Track point data are flattened to 2D (removing Z/elevation data).
- A list of subareas is created. If user has specified subareacode argument, a list with single value is created.
- An empty Dataframe with headers is created to store results of analysis.
- For each subarea:
    - Subset haulouts by subarea using SubareaCode value from column
    - Get sorted list of unique years surveyed within subset haulouts
    - Subset pseu-dopoints (anchors) by SubareaCode value
    - Duplicate anchor points for each year in unique years list add year column to pseudo-points dataframe with repeated years
    - Append the anchor points to the haulouts subset dataframe for processing
    - Group subset hauluts by year column
    - Clip coast line by region
    - Clip coast polygon by region
    - Apply negative buffer to clipped coast polygon
    - Convert clipped, buffered coast polygon to binary raster GeoTiff where land == 1 and water == 0
    - Create proximity raster from binary raster (cell values are distance in pixel units to nearest land pixel from binary 
    raster)
    - Create arrays from binary and proximity rasters
    - Create a graph from binary array, removing nodes that are on land (have value == 1 in array)
    - Add the proximity value to nodes using proximity array as 'weight' attribute in graph
    - Calculate edge weights `(sum([node1, node2]) / 2)` and add as 'weight' attribute in graph

    - For each unique survey year (if the same haulouts (SubsiteID) are in this year as previous, skip, otherwise...):
        - Get haulouts grouped by year
        - Get datetime results from nearest 10 records from modern survey track points
        - Select earliest datetime result from neighbours, add to haulouts dataframe
        - Sort haulouts by datetime and then add an 'order' field and assign value based on sorted dataframe
        - Move any haulouts that are on land to nearest location along coast line
        - Generate pixel coordinates for each haulout location to feed network shortest weighted path
        - Create paths along ordered haulouts using NetworkX shortest path using weighted graph
        - Get coordinates for each node in path
        - Create shapefile for ordered, moved haulouts
        - Create points and then lines for path and write to disk as shapefile
        - Buffer path by specified pathbuffer argument
        - Erase clipped coast line by buffered path
        - Calculate percent surveyed: `round((surveyed_length / coast_length) * 100, 2)`
        - Append results for subarea to overall results dataframe
	    - Generate a plot for the subarea with the haulouts, unsurveyed coast line, path, and buffered path.
- Write results dataframe to disk as csv
    

## Requirements
See environment.yml for package requirements.


## Caveats
Haulout locations located 'on land' (i.e., on a polygon from the Low Water geometry) are shifted to the nearest location 
on the coast line for analysis. This is to ensure that haulouts will have a node in the graph and that a path can be created 
connecting all the haulout locations in the subarea.

Anchor points were manually created to guide the paths in the SOG region's subareas. These were partly to guide the path to 
the end of the coastline within a subarea, or to guide the path around an island.


## Uncertainty
Values in output csv are rough estimates of shoreline covered. Values created are influened by input parameters such as 
cell resolution and buffer values. 


## Acknowledgements
 - Chad Nordstrom
 - Kayleigh Gillespie
 - Michelle Bigg
 - Robert Skelly
 - Jessica Nephin


## References
 - Sea Lion by Chanut is Industries from NounProject.com
 - Conditional log msg: https://stackoverflow.com/questions/34954373/disable-format-for-some-messages
 - Flatten geometry: https://github.com/shapely/shapely/issues/709
 - CKD Nearest: https://gis.stackexchange.com/questions/222315/finding-nearest-point-in-other-geodataframe-using-geopandas
 - Shapely Nearest: https://shapely.readthedocs.io/en/stable/manual.html#shapely.ops.nearest_points
 - Snap points: https://gis.stackexchange.com/questions/306838/snap-points-shapefile-to-line-shapefile-using-shapely
 - Assign value while iterating: https://stackoverflow.com/questions/23330654/update-a-dataframe-in-pandas-while-iterating-row-by-row
 - Round up nearest 10: https://stackoverflow.com/questions/26454649/python-round-up-to-the-nearest-ten
 - Vector to raster: https://stackoverflow.com/questions/42206195/use-geodataframe-as-a-osgeo-ogr-datasource
 - Proximity raster: https://gis.stackexchange.com/questions/300286/geodesic-distance-from-raster-in-python
 - Get coord list: https://stackoverflow.com/questions/23151246/iterrows-pandas-get-next-rows-value
 - XY from pixel coords: https://stackoverflow.com/questions/52443906/pixel-array-position-to-lat-long-gdal-python
 - Network edge weights: https://datascience.stackexchange.com/questions/61248/create-nodes-edges-from-csv-latitude-and-longitude-for-graphs
 - Plotting: https://medium.com/geekculture/plotting-maps-with-geopandas-and-contextily-8d4b1f02603d
 
